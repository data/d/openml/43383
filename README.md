# OpenML dataset: Superstore-Sales-Dataset

https://www.openml.org/d/43383

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Retail dataset of a global superstore for 4 years.
Perform EDA and Predict the sales of the next 7 days from the last date of the Training dataset!

Content
Time series analysis deals with time series based data to extract patterns for predictions and other characteristics of the data. It uses a model for forecasting future values in a small time frame based on previous observations. It is widely used for non-stationary data, such as economic data, weather data, stock prices, and retail sales forecasting.

Dataset
The dataset is easy to understand and is self-explanatory

Inspiration
Perform EDA and Predict the sales of the next 7 days from the last date of the Training dataset!

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43383) of an [OpenML dataset](https://www.openml.org/d/43383). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43383/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43383/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43383/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

